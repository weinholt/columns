
This is a standalone AI for the game [Columns][columns]. It is
described in [AI for the Columns Game][columns-game-ai]
and [Columns Game for Arduino][columns-game-arduino]. It can run
natively in GNU/Linux with ncurses or be loaded onto an Arduino
compatible board (although only ESP8266 works right now, patches
welcome).

  [columns]: https://en.wikipedia.org/wiki/Columns_(video_game)
  [columns-game-ai]: https://weinholt.se/articles/columns-game-ai/
  [columns-game-arduino]: https://weinholt.se/articles/columns-game-arduino/
