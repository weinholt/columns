#ifndef AI_H
#define AI_H

#include <stdint.h>

#define COLUMN_H 3              /* this is also hardcoded */
#define HIDDEN (COLUMN_H - 1)
#define BOARD_W 6
#define BOARD_H (14 + COLUMN_H)
#define JEWEL_REMOVE_MARK  0x80U
#define JEWEL_BASE '0'
#define JEWEL_CLEAR 0x00U
#define JEWEL_MAGIC '*'
#define JEWEL_COLORS 6

#ifdef __cplusplus
extern "C" {
#endif

extern uint8_t board[BOARD_W][BOARD_H];
extern uint8_t next_column[COLUMN_H];
extern int_fast32_t score;

void init_board(void);
int is_game_over(void);
void make_next_column(void);
void place_next_column(void);
int fall(void);
void rotate(void);
int remove_magic(void);
int remove_lines(void);
int_fast32_t remove_until_fixpoint();
void searching_ai(void);

#ifdef __cplusplus
}
#endif

#endif
