/* Columns AI
 * Copyright © 2016, 2018 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// -std=c11

#ifndef ARDUINO
#define HOSTED
#endif

#ifdef HOSTED
# define _DEFAULT_SOURCE 1
# include <stdlib.h>
# include <ncursesw/curses.h>
# include <locale.h>
# include <unistd.h>
# include <time.h>
#else
#define random cpp_random
long cpp_random(void);
#endif

#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <limits.h>

#include "ai.h"

// Board. Bytes '0'-'5' are different jewels.
static const int MAGIC_CHANCE = 1;
uint8_t board[BOARD_W][BOARD_H];
uint8_t next_column[COLUMN_H];
size_t column_x = 0, column_y = 0;
int_fast32_t score;

struct line {
    int8_t y0, x0, y1, x1;
} lines[] = {
    {.x0 = -1, .y0 = 1, .x1 =  1, .y1 = -1},  /* \ */
    {.x0 =  0, .y0 = 1, .x1 =  0, .y1 = -1},  /* | */
    {.x0 =  1, .y0 = 1, .x1 = -1, .y1 = -1},  /* / */
    {.x0 = -1, .y0 = 0, .x1 =  1, .y1 =  0},  /* - */
};

void init_board(void)
{
    memset(board, JEWEL_CLEAR, sizeof board);
}

static uint8_t board_ref(size_t y, size_t x)
{
    return board[x][y];
}

static void board_set(size_t y, size_t x, uint8_t v)
{
    board[x][y] = v;
}

static uint8_t safe_board_ref(ssize_t y, ssize_t x)
{
    if (y < 0 || y >= BOARD_H || x < 0 || x >= BOARD_W)
        return JEWEL_CLEAR;

    return board_ref(y, x);
}

int is_game_over(void)
{
    /* If any column can fall then the game is not over. */
    for (size_t y = BOARD_H - 1; y > 0; y--)
        for (size_t x = 0; x < BOARD_W; x++)
            if (board_ref(y, x) == JEWEL_CLEAR && board_ref(y - 1, x) != JEWEL_CLEAR)
                return 0;       /* the column can fall to line y */

    /* If no column can fall and there are still columns in the hidden
     * area -- game over. */
    for (size_t y = 0; y < COLUMN_H; y++)
        for (size_t x = 0; x < BOARD_W; x++)
            if (board_ref(y, x) != JEWEL_CLEAR)
                return 1;

    return 0;
}

void make_next_column(void)
{
    int magic = (random() % 100) < MAGIC_CHANCE;

    for (size_t y = 0; y < COLUMN_H; y++) {
        size_t column = magic ? JEWEL_MAGIC : JEWEL_BASE + random() % JEWEL_COLORS;

        next_column[y] = column;
    }
}

void place_next_column(void)
{
    size_t y, x = BOARD_W / 2;

    for (y = 0; y < COLUMN_H; y++) {
        board_set(y, x, next_column[y]);
    }

    column_x = x;
    column_y = COLUMN_H - 1;
}

/* Move the falling column to the given x column. If not possible,
 * nothing happens. */
void move_falling_column(ssize_t x)
{
    ssize_t y = column_y, old_x = column_x;

    if (x < 0 || x >= BOARD_W || x == old_x)
        return;

    if (board_ref(y, x) | board_ref(y - 1, x) | board_ref(y - 2, x))
        return;

    for (int i = 0; i < COLUMN_H; i++) {
        board_set(y - i, x, board_ref(y - i, old_x));
        board_set(y - i, old_x, JEWEL_CLEAR);
    }
    column_x = x;
}

int fall(void)
{
    int fallen = 0;

    for (size_t y = BOARD_H - 1; y > 0; y--) {
        for (size_t x = 0; x < BOARD_W; x++) {
            /* Check if there's a tile that can fall down to line y. */
            if (board_ref(y, x) == JEWEL_CLEAR && board_ref(y - 1, x) != JEWEL_CLEAR) {
                board_set(y, x, board_ref(y - 1, x));
                board_set(y - 1, x, JEWEL_CLEAR);
                if (y - 1 == column_y && column_x == x)  {
                    column_y = y;
                }
                fallen++;
            }
        }
    }

    return fallen;
}

void rotate(void)
{
    uint8_t a = board_ref(column_y, column_x),
        b = board_ref(column_y - 1, column_x),
        c = board_ref(column_y - 2, column_x);

    board_set(column_y, column_x, c);
    board_set(column_y - 1, column_x, a);
    board_set(column_y - 2, column_x, b);
}

/* Remove all magics and the tile they hit. */
int remove_magic(void)
{
    int tiles = 0;
    uint_fast8_t hit_tile = safe_board_ref(column_y + 1, column_x);

    /* If the magic has fallen, remove all tiles of the same color
     * as the one it hit. */
    if (board_ref(column_y, column_x) == JEWEL_MAGIC
        && (column_y == BOARD_H - 1 || hit_tile != JEWEL_CLEAR)) {
        for (int y = 0; y < BOARD_H; y++) {
            for (int x = 0; x < BOARD_W; x++) {
                if (board_ref(y, x) == JEWEL_MAGIC
                    || (column_y != BOARD_H - 1 && board_ref(y, x) == hit_tile)) {
                    tiles++;
                    board_set(y, x, JEWEL_CLEAR);
                }
            }
        }
    }

    return tiles;
}

/* Remove all lines in the shadow board. Returns the number of tiles
 * that were removed. */
int remove_lines(void)
{
    int tiles = 0;

    /* Mark all lines with JEWEL_REMOVE_MARK. */
    for (size_t y = 0; y < BOARD_H; y++) {
        for (size_t x = 0; x < BOARD_W; x++) {
            for (size_t l = 0; l < sizeof(lines) / sizeof(*lines); l++) {
                uint8_t b = board_ref(y, x) & ~JEWEL_REMOVE_MARK;

                if (b == JEWEL_CLEAR)
                    continue;

                ssize_t ay = y + lines[l].y0, ax = x + lines[l].x0,
                    cy = y + lines[l].y1, cx = x + lines[l].x1;

                uint8_t a = safe_board_ref(ay, ax) & ~JEWEL_REMOVE_MARK,
                    c = safe_board_ref(cy, cx) & ~JEWEL_REMOVE_MARK;

                if (a == b && b == c) {
                    board_set(ay, ax, a | JEWEL_REMOVE_MARK);
                    board_set(y, x, b | JEWEL_REMOVE_MARK);
                    board_set(cy, cx, c | JEWEL_REMOVE_MARK);
                }
            }
        }
    }

    /* Remove all marked tiles. */
    for (size_t y = 0; y < BOARD_H; y++)
        for (size_t x = 0; x < BOARD_W; x++)
            if (board_ref(y, x) & JEWEL_REMOVE_MARK) {
                board_set(y, x, JEWEL_CLEAR);
                tiles++;
            }

    return tiles;
}

/* Remove tiles due to magics and lines, until everything settles. The
 * column must have stopped falling before this is called. */
int_fast32_t remove_until_fixpoint()
{
    int_fast32_t score = remove_magic();
    int removed;
    int multiplier = 1;

    while (fall() > 0)
        ;
    if (!is_game_over()) {
        while ((removed = remove_lines())) {
            score += removed * multiplier;
            multiplier *= 2;
            while (fall() > 0)
                ;
        }
    }

    return score;
}

void searching_ai(void)
{
    size_t best_x = 0, best_rotate = 0;
    int_fast32_t best_score = INT_FAST32_MIN;

    /* Search strategy:
     * - For each x:
     * -- For each rotation:
     * --- Compute the score.
     * --- For each x with the next column:
     * ---- For each rotation with the next column:
     * ----- Compute the score and add to earlier score.
     * ------ For each x with the hypothetical column:
     * ------- For each rotation with the hypothetical column:
     * -------- Compute the score and add to earlier score.
     */

    /* Save everything so it can be restored. */
    uint8_t backup_x = column_x, backup_y = column_y;
    uint8_t backup_board[BOARD_W][BOARD_H];
    memcpy(backup_board, board, sizeof board);

    size_t startx = random() % BOARD_W;
    for (size_t i = startx; i < startx + BOARD_W; i++) {
        size_t x0 = i % BOARD_W;
        for (size_t rotations0 = 0; rotations0 < COLUMN_H; rotations0++) {
            move_falling_column(x0);
            for (size_t i = 0; i < rotations0; i++)
                rotate();
            while (fall() > 0)
                ;               /* this can be optimized */
            int_fast32_t score0 = remove_until_fixpoint();

            if (!is_game_over()) {
                place_next_column();
                for (size_t x1 = 0; x1 < BOARD_W; x1++) {
                    for (size_t rotations1 = 0; rotations1 < COLUMN_H; rotations1++) {
                        /* Second backup copy. XXX: To use less
                         * memory, restore from backup_board and
                         * recompute the board using the code above
                         * the loop. */
                        uint8_t backup2_x = backup_x, backup2_y = backup_y;
                        uint8_t backup2_board[BOARD_W][BOARD_H];
                        memcpy(backup2_board, board, sizeof board);

                        move_falling_column(x1);
                        for (size_t i = 0; i < rotations1; i++)
                            rotate();
                        while (fall() > 0)
                            ;   /* this can be optimized */
                        int_fast32_t score1 = remove_until_fixpoint();

                        if (!is_game_over()) {
                            int_fast32_t score = score0 + score1;

                            if (score > best_score) {
                                best_x = x0;
                                best_rotate = rotations0;
                                best_score = score;
                            }
                        }

                        memcpy(board, backup2_board, sizeof board);
                        column_x = backup2_x; column_y = backup2_y;
                    }
                }
            }

            memcpy(board, backup_board, sizeof board);
            column_x = backup_x; column_y = backup_y;
        }
    }

    /* Move and rotate. */
    move_falling_column(best_x);
    for (size_t i = 0; i < best_rotate; i++)
        rotate();
}

#ifdef HOSTED

void print_initial_board(void)
{
    /* Draw lines between the columns (just shows what is hidden. */
    for (int y = 0; y < BOARD_H; y++) {
        for (int x = 0; x < BOARD_W - 1; x++) {
            if (attron(COLOR_PAIR(y < COLUMN_H ? '-' : '+')) == ERR) goto error;
            if (mvaddch(y, x * 2 + 1, JEWEL_CLEAR) == ERR) goto error;
        }
    }

    return;
error:
    abort();
}

void print_board(void)
{
    for (int y = 0; y < COLUMN_H; y++) {
        uint8_t jewel = next_column[y];
        jewel = jewel == JEWEL_CLEAR ? ' ' : jewel;
        if (attron(COLOR_PAIR(jewel)) == ERR) goto error;
        if (mvaddch(y + 1, (BOARD_W + 1) * 2, jewel) == ERR) goto error;
    }
    for (int y = 0; y < BOARD_H; y++) {
        for (int x = 0; x < BOARD_W; x++) {
            uint8_t jewel = board_ref(y, x);
            jewel = jewel == JEWEL_CLEAR ? ' ' : jewel;
            if (attron(COLOR_PAIR(jewel)) == ERR) goto error;
            if (mvaddch(y, x * 2, jewel) == ERR) goto error;
        }
    }

    char msg[100];
    snprintf(msg, sizeof msg, "%2ld/%2ld   %5ld", column_y, column_x, score);
    attron(COLOR_PAIR(JEWEL_MAGIC));
    mvaddnstr(2, (BOARD_W + 3) * 2, msg, sizeof msg);

    if (refresh() == ERR) goto error;

    return;

error:
    abort();
}

int main(int argc, char *argv[])
{
    srandom(time(NULL));
    setlocale(LC_ALL, "");

    /* Initialize ncurses. */
    initscr(); cbreak(); noecho();
    nonl();
    intrflush(stdscr, FALSE);
    keypad(stdscr, TRUE);
    curs_set(0);

    /* Get colors working. */
    start_color();
    init_pair(JEWEL_BASE + 0, COLOR_YELLOW, COLOR_BLACK);
    init_pair(JEWEL_BASE + 1, COLOR_BLUE, COLOR_BLACK);
    init_pair(JEWEL_BASE + 2, COLOR_GREEN, COLOR_BLACK);
    init_pair(JEWEL_BASE + 3, COLOR_RED, COLOR_BLACK);
    init_pair(JEWEL_BASE + 4, COLOR_WHITE, COLOR_BLACK);
    init_pair(JEWEL_BASE + 5, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(JEWEL_MAGIC, COLOR_RED, COLOR_WHITE);
    init_pair(' ', COLOR_BLACK, COLOR_BLACK);
    init_pair('-', COLOR_BLUE, COLOR_BLUE);
    init_pair('+', COLOR_BLACK, COLOR_BLACK);

    /* Start the game. */
    init_board();
    make_next_column();
    place_next_column();
    make_next_column();
    searching_ai();
    print_initial_board();
    print_board();

    /* Game loop. */
    while (1) {
        usleep(1 * 1000);
        if (fall() == 0) {
            score += remove_until_fixpoint();
            if (!is_game_over()) {
                place_next_column();
                make_next_column();
                searching_ai();
            }
        }

        print_board();
    }

    endwin();
}
#endif
