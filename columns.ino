/* Columns AI                                          -*- mode: c++ -*-
 * Copyright © 2016, 2018 Göran Weinholt <goran@weinholt.se>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <NeoPixelBus.h>
#include <limits.h>

#include "ai.h"

// Neopixel displays
const uint16_t PixelCount = 8*8*2;
const uint8_t PixelPin = D4;
NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart800KbpsMethod> strip(PixelCount);

const int PanelWidth = 8;
const int PanelHeight = 8;
const int TileWidth = 1;
const int TileHeight = 2;
NeoTiles <ColumnMajorLayout, RowMajorLayout> tiles(
    PanelWidth,
    PanelHeight,
    TileWidth,
    TileHeight);

extern "C" long cpp_random(void);
long cpp_random(void)
{
    return random(LONG_MAX);
}

void SetRandomSeed()            // from Makuna/NeoPixelBus
{
  uint32_t seed;

  // random works best with a seed that can use 31 bits
  // analogRead on a unconnected pin tends toward less than four bits
  seed = analogRead(0);
  delay(1);

  for (int shifts = 3; shifts < 31; shifts += 3)
  {
    seed ^= analogRead(0) << shifts;
    delay(1);
  }

  randomSeed(seed);
}


void setup() {
    Serial.begin(115200);
    Serial.println("Booting");
    Serial.print("Reset reason: ");
    Serial.println(ESP.getResetReason());

    init_board();
    make_next_column();
    place_next_column();
    make_next_column();
    searching_ai();

    // Neopixel strip.
    strip.Begin();
    strip.ClearTo(RgbColor(0, 0, 0));
    strip.Show();
    SetRandomSeed();
}

void draw_jewel(size_t y, size_t x, uint8_t jewel, float magic_hue) {
    int L = 3;
    auto color = RgbColor{0, 0, 0};

    switch (jewel) {
    case JEWEL_BASE + 0:
        color = RgbColor{L, 0, 0};
        break;
    case JEWEL_BASE + 1:
        color = RgbColor{L, L, 0};
        break;
    case JEWEL_BASE + 2:
        color = RgbColor{L, 0, L};
        break;
    case JEWEL_BASE + 3:
        color = RgbColor{0, L, 0};
        break;
    case JEWEL_BASE + 4:
        color = RgbColor{L, L, L};
        break;
    case JEWEL_BASE + 5:
        color = RgbColor{0, 0, L};
        break;
    case JEWEL_MAGIC:
        color = HslColor(magic_hue, 1.0f, 0.05);
        break;
    case JEWEL_CLEAR:
    default:
        break;
    }

    strip.SetPixelColor(tiles.Map(x, y), color);
}

void render_board(uint8_t shown_board[BOARD_W][BOARD_H], float magic_hue) {
    for (size_t y = 0; y < BOARD_H - HIDDEN; y++) {
        for (size_t x = 0; x < BOARD_W; x++) {
            draw_jewel(y + 1, 7-x, shown_board[x][y + HIDDEN], magic_hue);
        }
    }
    strip.Show();
}

void loop() {
    static float magic_hue = 0.0f;
    static int iteration = 0;
    static uint8_t prev_board[BOARD_W][BOARD_H];

    magic_hue = magic_hue + 0.01f;
    if (magic_hue >= 1.0f)
        magic_hue = 0.0f;
    iteration = (iteration + 1) % 16;
    delay(1);

    // Game logic
    if (iteration == 0 && fall() == 0) {
        memcpy(prev_board, board, sizeof board);
        prev_board[1][1] = 3;
        if (remove_magic()) {
            for (int i = 0; i < 3; i++)   {
                render_board(board, magic_hue);
                delay(100);
                render_board(prev_board, magic_hue);
                delay(100);
            }
        }

        while (fall() > 0) {
            render_board(board, magic_hue);
            delay(50);
        }

        memcpy(prev_board, board, sizeof board);
        while (remove_lines()) {
            for (int i = 0; i < 3; i++)   {
                render_board(board, magic_hue);
                delay(100);
                render_board(prev_board, magic_hue);
                delay(100);
            }

            while (fall() > 0) {
                render_board(board, magic_hue);
                delay(50);
            }
            memcpy(prev_board, board, sizeof board);
        }

        if (!is_game_over()) {
            place_next_column();
            make_next_column();
            searching_ai();
        }
    }

    // Render the whole screen.
    for (size_t y = 0; y < COLUMN_H; y++)
        draw_jewel(y + 2, 7-(BOARD_W + 1), next_column[y], magic_hue);

    render_board(board, magic_hue);
}
